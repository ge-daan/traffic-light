from adapt.intent import IntentBuilder
from mycroft.skills.core import MycroftSkill, intent_handler, intent_file_handler
from mycroft.util.log import LOG
from mycroft.util.parse import extract_number, normalize

import random
import re

class trafficLightSkill(MycroftSkill):

    def __init__(self):
        super(trafficLightSkill, self).__init__(name="trafficLightSkill")

    #Respond to the user asking how long he has to wait before the lught turns green with a random amount seconds
    @intent_handler(IntentBuilder('').require('Moving_options').optionally('Query').optionally('Traffic_colour'))
    def handle_travel_advice(self, message):
        utt = message.data.get('utterance', "").lower()
        seconds = random.randrange(2, 10)
        #self.speak_dialog("you have to wait {} seconds before you can cross".format(seconds))
        self.speak_dialog("waiting.time", data={"time" : seconds})

def create_skill():
    return trafficLightSkill()
